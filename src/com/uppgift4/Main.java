package com.uppgift4;


public  class Main implements Runnable {

    private final int start;
    private final int slut;

    public Main(int start, int slut) {
        this.start = start;
        this.slut = slut;
    }

    @Override
    public void run() {
        for (int i = start; i<slut; i++){
            if(isPrime(i)){
                System.out.println(Thread.currentThread().getName() + " : " + i);
            }
        }

    }

    private boolean isPrime(int a) {
        if(a==1 ||a == 0) return false;

        for (int i = 2;i<a; i++ ){
            if(a%i==0) return false;
        }
        return true;
    }


    public static void main(String[] args) {

        Runnable runnable1 = new Main(0, 100000);
        Thread thread1 = new Thread(runnable1);
        thread1.start();

        Runnable runnable2 = new Main(100001, 300000);
        Thread thread2 = new Thread(runnable2);
        thread2.start();

        Runnable runnable3 = new Main(300001, 500000);
        Thread thread3 = new Thread(runnable3);
        thread3.start();

    }


}