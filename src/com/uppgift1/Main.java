package com.uppgift1;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Main {
    public static void main(String[] args) {



        List<Person> person = List.of(
                new Person("Edin", "MALE", 29000.00),
                new Person("Anna", "FEMALE", 33000.00),
                new Person("Erik", "MALE", 45000.00),
                new Person("Adam", "MALE", 22000.00),
                new Person("Erika", "FEMALE", 31000.00),
                new Person("Hanna", "FEMALE", 31200.00),
                new Person("Tina", "FEMALE", 24000.00),
                new Person("Martina", "FEMALE", 55000.00),
                new Person("Artina", "FEMALE", 60000.00),
                new Person("Peter", "MALE", 6500.00)

        );

        Map<String, Double> averageAgeMaleAndFemale = person.stream()
                .collect(Collectors.groupingBy(Person::getGender, Collectors.averagingDouble(Person::getSalary)));
        System.out.println(averageAgeMaleAndFemale);

        System.out.println(person.stream()
                .collect(Collectors.groupingBy(Person::getGender, Collectors.averagingDouble(Person::getSalary))));

        System.out.println(person.stream()
                .max(Comparator.comparing(Person::getSalary)));
        System.out.println(person.stream()
                .min(Comparator.comparing(Person::getSalary)));


    }


}
