package com.uppgift2;

public class Main2 {
    public static void main(String[] args) {

        BilFabrik bilFabrik = new BilFabrik();

        ICar car1= bilFabrik.getMake("Ferrari");
        car1.make();

        ICar car2 = bilFabrik.getMake("Lamborghini");
        car2.make();

        ICar car3 = bilFabrik.getMake("Fiat");
        car3.make();

    }
}
