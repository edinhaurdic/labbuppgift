package com.uppgift2;

import java.util.List;

public class BilFabrik {

    public ICar getMake(String make) {
        if (make == null) {
            return null;
        }
        if (make.equalsIgnoreCase("ferrari")) {
            return new Ferrari();
        } else if (make.equalsIgnoreCase("LAMBORGHINI")) {
            return new Lamborghini();
        } else if (make.equalsIgnoreCase("Fiat")) {
            return new Fiat();
        }

        return null;
    }
}
