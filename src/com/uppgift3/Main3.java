package com.uppgift3;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main3 {
    public static void main(String[] args) {

        Predicate<String> pattern = Pattern.compile("[aeiouy]",Pattern.CASE_INSENSITIVE)
                .asPredicate();



        //skapar en lista
        List<String> Word= List.of(
                "EE", "AA", "II", "Uu", "iI","Adam", "mm", "NN", "DRDR","Im", "IN", "Mimimi", "BEbebebebe"
        );

        Stream<String> answerList= Word.stream()
                .filter(pattern);

        answerList.forEach(System.out::println);



    }
}
